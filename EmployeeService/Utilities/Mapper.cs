﻿using PervasiveEmployeeModels = EmployeeService.Directories.PervasiveBuilding.Employee.Models;
using DataModels = EmployeeService.Data.Models;
using EmployeeService.Directories.PervasiveBuilding.Employee.Enum;

namespace EmployeeService.Utilities
{
   public class Mapper
   {
      public static PervasiveEmployeeModels.Employee Map(DataModels.Employee employee)
      {
         return new PervasiveEmployeeModels.Employee()
         {
            Id = employee.Id,
            Name = employee.Name,
            Surname = employee.Surname,
            Email = employee.Email,
            PreferredTemperature = employee.PreferredTemperature,
            Role = (EmployeeRole)employee.Role,
            RoomId = employee.RoomId,
         };
      }
   }
}
