﻿using AgileMQ.Interfaces;
using AgileMQ.Parameters;
using System;

namespace EmployeeService.Logger
{
   public class ConsoleLogger : IAgileLogger
   {
      public void Log(OnRpcRequest data)
      {
         Console.WriteLine("[" + data.SendDate + "] " + "Recieved a " + data.RpcMethod + " from: " + data.AppId + ": " + data.Message);
         Console.WriteLine();
      }

      public void Log(OnRpcResponse data)
      {
         Console.WriteLine("[" + data.SendDate + "] " + data.RpcMethod + " from " + data.AppId + " goes fine.");
         Console.WriteLine();
      }

      public void Log(OnRpcError data)
      {
         if (data.RetryIndex == data.RetryLimit)
         {
            Console.WriteLine("[" + data.SendDate + "] An error occurred on " + data.RpcMethod + " from " + data.AppId + ", with this message : " + data.Message);
            Console.WriteLine();
         }
      }

      public void Log(OnNotify data)
      {
         Console.WriteLine("[" + data.SendDate + "] " + data.EventMethod + " from: " + data.AppId + " : " + data.Message);
         Console.WriteLine();
      }

      public void Log(OnNotifyConsumed data)
      {
         Console.WriteLine("[" + data.SendDate + "] " + data.AppId + " consumed the event " + data.EventMethod);
         Console.WriteLine();
      }

      public void Log(OnNotifyError data)
      {
         if (data.RetryIndex == data.RetryLimit)
         {
            Console.WriteLine("[" + data.SendDate + "] An error occurred on " + data.EventMethod + " event, from " + data.AppId + ", with this message : " + data.Message);
            Console.WriteLine();
         }
      }
   }
}
