﻿using AgileMQ.Bus;
using AgileMQ.Enums;
using AgileMQ.Interfaces;
using EmployeeService.Data;
using EmployeeService.Logger;
using EmployeeService.Subscribers;
using System;

namespace EmployeeService
{
    public class Program
    {
      static void Main(string[] args)
      {
         using (IAgileBus bus = new RabbitMQBus("HostName=localhost;Port=5672;UserName=guest;Password=guest;Timeout=3;RetryLimit=3;PrefetchCount=50;AppId=EmployeeService"))
         {
            bus.Logger = new ConsoleLogger();

            bus.Container.Register<DataContext, DataContext>(InjectionScope.Message);

            bus.Suscribe(new EmployeeSubscriber());

            Console.WriteLine("EmployeeService Ready!");
            Console.WriteLine();
            Console.ReadLine();
         }
      }
   }
}
