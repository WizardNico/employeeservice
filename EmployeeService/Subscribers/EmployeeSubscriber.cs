﻿using AgileMQ.Interfaces;
using EmployeeService.Directories.PervasiveBuilding.Employee.Models;
using System;
using System.Collections.Generic;
using DataModels = EmployeeService.Data.Models;
using AgileMQ.Containers;
using AgileMQ.DTO;
using System.Threading.Tasks;
using EmployeeService.Data;
using EmployeeService.Data.Exceptions;
using EmployeeService.Utilities;
using EmployeeService.Data.Enums;
using AgileMQ.Extensions;
using EmployeeService.Data.Repositories;
using EmployeeService.Directories.PervasiveBuilding.Room.Models;

namespace EmployeeService.Subscribers
{
   public class EmployeeSubscriber : IAgileSubscriber<Employee>
   {
      public IAgileBus Bus { get; set; }

      public async Task<Employee> GetAsync(Employee model, MessageContainer container)
      {
         DataContext ctx = container.Resolve<DataContext>();

         DataModels.Employee employee = await ctx.Employees.FindByIdAsync(model.Id.Value);

         if (employee == null)
            throw new ObjectNotFoundException("Employee not found, a wrong ID was used.");

         model = Mapper.Map(employee);

         return (model);
      }

      public async Task<Employee> PostAsync(Employee model, MessageContainer container)
      {
         DataContext ctx = container.Resolve<DataContext>();

         //Get the indicated room
         Room room = new Room()
         {
            Id = model.RoomId.Value
         };
         room = await Bus.GetAsync(room);

         if (room == null)
            throw new ObjectNotFoundException("Room not found, a wrong ID was used.");

         //Cretion of new temperature record
         DataModels.Employee employee = new DataModels.Employee()
         {
            Name = model.Name,
            Surname = model.Surname,
            RoomId = room.Id.Value,
            Email = model.Email,
            PreferredTemperature = model.PreferredTemperature.Value,
            Role = (EmployeeRole)model.Role,
         };

         //Add the new record to DB adn save changes
         await ctx.Employees.AddAsync(employee);
         await ctx.SaveChangesAsync();

         model = Mapper.Map(employee);

         return (model);
      }

      public async Task PutAsync(Employee model, MessageContainer container)
      {
         DataContext ctx = container.Resolve<DataContext>();

         DataModels.Employee employee = await ctx.Employees.FindByIdAsync(model.Id.Value);

         if (employee == null)
            throw new ObjectNotFoundException("Employee not found, a wrong ID was used.");

         //Get the indicated room
         Room room = new Room()
         {
            Id = model.RoomId.Value
         };
         room = await Bus.GetAsync(room);

         if (room == null)
            throw new ObjectNotFoundException("Room not found, a wrong ID was used.");


         employee.Name = model.Name;
         employee.Surname = model.Surname;
         employee.RoomId = room.Id.Value;
         employee.Email = model.Email;
         employee.PreferredTemperature = model.PreferredTemperature.Value;
         employee.Role = (EmployeeRole)model.Role;

         await ctx.SaveChangesAsync();
      }

      public async Task DeleteAsync(Employee model, MessageContainer container)
      {
         DataContext ctx = container.Resolve<DataContext>();

         DataModels.Employee employee = await ctx.Employees.FindByIdAsync(model.Id.Value);

         if (employee == null)
            throw new ObjectNotFoundException("Employee not found, a wrong ID was used.");

         ctx.Employees.Remove(employee);
         await ctx.SaveChangesAsync();
      }

      public async Task<List<Employee>> GetListAsync(Dictionary<string, object> filter, MessageContainer container)
      {
         DataContext ctx = container.Resolve<DataContext>();

         long? roomId = filter.Get<long?>("roomId");
         double? preference = filter.Get<double?>("preference");
         EmployeeRole? role = filter.Get<EmployeeRole?>("role");

         List<Employee> list = new List<Employee>();

         List<DataModels.Employee> employeeList = await ctx.Employees.GetListAsync(preference, role, roomId);

         foreach (DataModels.Employee employee in employeeList)
         {
            list.Add(Mapper.Map(employee));
         }

         return (list);
      }

      public Task<Page<Employee>> GetPagedListAsync(Dictionary<string, object> filter, long pageIndex, int pageSize, MessageContainer container)
      {
         throw new NotImplementedException();
      }
   }
}
