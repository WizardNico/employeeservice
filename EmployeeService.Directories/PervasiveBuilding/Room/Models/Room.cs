﻿using AgileMQ.Attributes;
using EmployeeService.Directories.PervasiveBuilding.Room.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EmployeeService.Directories.PervasiveBuilding.Room.Models
{
   [QueuesConfig(Directory = "PervasiveBuilding", Subdirectory = "Room", ResponseEnabled = true)]
   public class Room
   {
      [Required]
      public long? Id { get; set; }



      [Required]
      public int? RoomNumber { get; set; }

      [Required]
      public int? FloorNumber { get; set; }



      [Required]
      public long? FireEscapeId { get; set; }

      public FireEscape FireEscape { get; set; }
   }
}
