﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmployeeService.Data.Exceptions
{
   public class ObjectNotFoundException : Exception
   {
      public ObjectNotFoundException(string message) : base(message) { }
   }
}
