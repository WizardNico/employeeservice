﻿using EmployeeService.Data.Enums;
using EmployeeService.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeService.Data.Repositories
{
   public static class IComparableExtension
   {
      public static bool InRange<T>(this T value, T from, T to) where T : IComparable<T>
      {
         return value.CompareTo(from) >= 1 && value.CompareTo(to) <= -1;
      }
   }

   public static class EmployeeRepository
   {
      public static async Task<Employee> FindByIdAsync(this DbSet<Employee> repository, long id)
      {
         Employee employee = await repository
           .Where(emp => emp.Id == id)
           .SingleOrDefaultAsync();

         return (employee);
      }

      public static async Task<List<Employee>> GetListAsync(this DbSet<Employee> repository, double? preference, EmployeeRole? role, long? roomId)
      {
         IQueryable<Employee> query = repository;

         if (roomId != null)
         {
            query = query.Where(emp => emp.RoomId == roomId);
         }

         if (preference != null)
         {
            query = query.Where(emp => preference.Value.InRange(emp.PreferredTemperature - 0.6, emp.PreferredTemperature + 0.6));
         }

         if (role != null)
         {
            query = query.Where(emp => emp.Role == role);
         }

         List<Employee> result = await query
            .ToListAsync();

         return (result);
      }
   }
}
