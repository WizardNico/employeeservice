﻿using EmployeeService.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EmployeeService.Data.Models
{
   public class Employee
   {
      public long Id { get; set; }



      public EmployeeRole Role { get; set; }

      [Required]
      public string Name { get; set; }

      [Required]
      public string Surname { get; set; }

      [Required]
      [EmailAddress]
      public string Email { get; set; }

      public double PreferredTemperature { get; set; }



      public long RoomId { get; set; }
   }
}
