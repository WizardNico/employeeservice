﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace EmployeeService.Data.Migrations
{
   public partial class SqlScript : Migration
   {
      protected override void Up(MigrationBuilder migrationBuilder)
      {
         //Insert employee
         migrationBuilder.Sql("INSERT INTO Employees (Email, Name, PreferredTemperature, Role, RoomId, Surname) VALUES ('andrea.nicolini93@gmail.com','Andrea', 20, 10, 5, 'Nicolini')");
         migrationBuilder.Sql("INSERT INTO Employees (Email, Name, PreferredTemperature, Role, RoomId, Surname) VALUES ('luca.nicolini85@gmail.com','Luca', 25, 30, 5, 'Nicolini')");
         migrationBuilder.Sql("INSERT INTO Employees (Email, Name, PreferredTemperature, Role, RoomId, Surname) VALUES ('william.nicolini57@gmail.com','William', 20.5, 10, 5, 'Nicolini')");
      }

      protected override void Down(MigrationBuilder migrationBuilder)
      {

      }
   }
}
