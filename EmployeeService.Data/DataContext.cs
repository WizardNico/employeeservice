﻿using EmployeeService.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmployeeService.Data
{
   public class DataContext : DbContext
   {
      public DbSet<Employee> Employees { get; set; }

      protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
      {
         optionsBuilder.UseSqlServer(@"Data Source=localhost;Initial Catalog=EmployeeService;Persist Security Info=True;User ID=sa;Password=nico93;Pooling=False;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True");
      }

      protected override void OnModelCreating(ModelBuilder modelBuilder)
      {
         modelBuilder.Entity<Employee>()
            .HasIndex(emp => new { emp.Email })
            .IsUnique(true);
      }
   }
}
